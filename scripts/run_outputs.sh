#!/bin/bash

# File: run_outputs.sh 
# Author(s): Saswati Ray
# Created: Wed Feb 17 06:44:20 EST 2021 
# Description:
# Acknowledgements:
# Copyright (c) 2021 Carnegie Mellon University
# This code is subject to the license terms contained in the code repo.

rm -rf ${D3MOUTPUTDIR}/* > /dev/null 2>&1
/d3m/src/main.py search

# code to score the output, and write to the $D3MOUTPUTDIR directory
for search_dir in ${D3MOUTPUTDIR}/*
do
   echo ${search_dir}
   for jsonfile in ${search_dir}/pipelines_ranked/*
   do
      echo ${jsonfile}
      echo ${jsonfile} > jsonfilename.txt
      prediction_name=`awk -F'/' '{print $NF}' jsonfilename.txt`
      mkdir ${search_dir}/pipeline_runs/${prediction_name}
      python3 -m d3m runtime -v ${D3MSTATICDIR} fit-produce -E ${search_dir}/pipeline_runs/${prediction_name} -p ${jsonfile} -r ${D3MINPUTDIR}/TRAIN/problem_TRAIN/problemDoc.json -i ${D3MINPUTDIR}/TRAIN/dataset_TRAIN/datasetDoc.json -t ${D3MINPUTDIR}/TEST/dataset_TEST/datasetDoc.json -o ${search_dir}/predictions/${prediction_name}.predictions.csv
   python3 /scripts/mkpline.py ${jsonfile} gencode.py
   mv gencode.py ${search_dir}/executables/${prediction_name}.code.py 
   done
done
