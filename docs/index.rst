.. image:: img/AutonML_logo.png
   :scale: 50%
   :alt: CMU TA2 Pipeline

Welcome to the documentation site for CMU TA2 Auto\ :sup:`n`\ML. 

Built using DARPA D3M ecosystem

Overview
--------
Auto\ :sup:`n`\ML is an automated machine learning system developed by CMU Auton Lab 
to power data scientists with efficient model discovery and advanced data analytics. 
It is designed to be the first step in exploratory data science. 
Auto\ :sup:`n`\ML leverages powerful methods in machine learning to 
solve an enriched array of tasks on different data modalities.

.. image:: img/model_pipeline.png
   :scale: 50%
   :alt: CMU TA2 Pipeline

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Getting Started
   :name: sec-getting_started

   getting_started
   convert_d3m_data

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Auton ML Organization
   :name: sec-organization

   functionalities

