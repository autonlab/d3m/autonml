#!/bin/bash

#pip install -r requirements.txt

# Tabular Datasets
INPUT_DIR=/home/vsanil/projects/d3m/datasets/185_baseball_MIN_METADATA
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/uu6_hepatitis_MIN_METADATA

# Text Datasets
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/LL1_TXT_CLS_airline_opinion_MIN_METADATA

# Image Datasets
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/22_handgeometry_MIN_METADATA

# Time Series Datasets
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/56_sunspots_MIN_METADATA
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/56_sunspots_monthly_MIN_METADATA

# Video Datasets
#INPUT_DIR=/home/vsanil/projects/d3m/datasets/LL1_3476_HMDB_actio_recognition_MIN_METADATA

OUTPUT_DIR=/home/vsanil/projects/d3m/devel/cmu-ta2/output
TIMEOUT=10
NUMCPUS=8
PROBLEMPATH=${INPUT_DIR}/TRAIN/problem_TRAIN/problemDoc.json

autonml_main ${INPUT_DIR} ${OUTPUT_DIR} ${TIMEOUT} ${NUMCPUS} ${PROBLEMPATH} 

#python -m autonml.main ${INPUT_DIR} ${OUTPUT_DIR} ${TIMEOUT} ${NUMCPUS} ${PROBLEMPATH}
