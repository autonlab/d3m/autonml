# File: Dockerfile 
# Author(s): Saswati Ray
# Created: Wed Feb 17 06:44:20 EST 2021 
# Description:
# Acknowledgements:
# Copyright (c) 2021 Carnegie Mellon University
# This code is subject to the license terms contained in the code repo.

# JPL base image
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-stable-20201201-223410
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-devel
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-master-20210616-060925
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python38-master-20211129-213248
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python38-devel-20211202-082026
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python38-devel-20211209-093728
#FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python38-master-20211212-181648 
FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python38-master-20220120-204212

maintainer "Saswati Ray<sray@cs.cmu.edu>"

user root

# add git-lfs gpg and return exit code 0
RUN apt-get update || (apt-get install dirmngr && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 762E3157)

# libcurl4-openssl-dev for pycurl
# python3-tk for d3m.index
RUN sudo apt-get update && apt-get install -y \
    libcurl4-openssl-dev \
    python3-tk

## install d3m and grpc, a D3M dependency
##
## We use pip==18.1 because pip 19+ removed --process-dependency-links
##
RUN pip3 install --upgrade pip==18.1 \
#    && python3 -m pip install --process-dependency-links d3m \
    && python3 -m pip install --upgrade grpcio grpcio-tools


#RUN python3 -m pip install --exists-action w -e git+https://gitlab.com/awilliams-cmu/d3m.git@fix-slow-cols#egg=d3m 

EXPOSE 45042

RUN mkdir /d3m
ADD src/ /d3m/src

CMD /d3m/src/main.py ${D3MRUN}
