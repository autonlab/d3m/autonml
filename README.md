<img src="https://gitlab.com/sray/cmu-ta2/-/raw/master/docs/img/AutonML_logo.png?inline=false" width=30%>


# CMU AutonML (Built using DARPA D3M ecosystem)

**Taking your machine learning capacity to the nth power.**

Auto<sup>n</sup>ML is an automated machine learning system developed by CMU Auton Lab 
to power data scientists with efficient model discovery and advanced data analytics. It is designed to be the first step in exploratory data science. Auto<sup>n</sup>ML leverages powerful methods in machine learning to solve an enriched array of tasks on different data modalities.

Auto<sup>n</sup>ML is Carnegie Mellon University's implementation of the [Data Driven Discovery program (D3M)](https://datadrivendiscovery.org/).

[Documentation is available here](https://autonml.readthedocs.io/en/latest/)
